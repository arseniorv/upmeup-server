import { Injectable, Scope, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Client, } from "minio"

@Injectable({ scope: Scope.DEFAULT })
export class StorageService {
    private client: Client;
    private bucket: string

    constructor(private configService: ConfigService) {
        
        
        this.client = new Client({
            useSSL: configService.get('S3_SSL') === 'true' ? true : false,
            endPoint: configService.get('S3_ENDPOINT'),
            port: parseInt(configService.get('S3_PORT')),
            accessKey: configService.get('S3_ACCESS_KEY'),
            secretKey: configService.get('S3_SECRET_KEY')

        })
        this.bucket = configService.get('S3_BUCKET');
        
        // Log
        Logger.log(`StorageService : init : Bucket : ${this.bucket}`)
        Logger.debug(this.client)
        
    }
    
    async checkObjectExists(id: string, filename: string) {
        
        try {
            Logger.debug(`StorageService : checkObjectExists : ${id}/${filename}`)
            await this.client.statObject(this.bucket, `${id}/${filename}`);
            return true
        } catch (error) {
            return false;
        }
    }

    async getPresignedDownloadUrl(id: string, filename:string) {
        Logger.debug(`StorageService : getPresignedDownloadUrl : ${id}/${filename}`)
        return await this.client.presignedGetObject(this.bucket, `${id}/${filename}`, 60*30);
    }

    async getPresignedUploadUrl(id: string, filename: string) {
        Logger.log(`StorageService : getPresignedUploadUrl : ${id}/${filename}`)
        return await this.client.presignedPutObject(this.bucket, `${id}/${filename}`, 60*30);
    }

    async removeObject(id: string, filename: string) {
        Logger.debug(`StorageService : removeObject : ${id}/${filename}`)
        return await this.client.removeObject(this.bucket, `${id}/${filename}`);
    }
}
