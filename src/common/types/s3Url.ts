import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'presigned url for s3 storage' })
export class S3Url {
  @Field(() => String)
  presignedUrl: string;
}
