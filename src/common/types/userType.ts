export enum UserType {
    CANDIDATE = 'candidate',
    COMPANY = 'company',
    ADMIN = 'admin_user'
}