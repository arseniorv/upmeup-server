export class MessageNotification {
    chatUserId: string;
    chatRoomId: string;
    message: string;
}