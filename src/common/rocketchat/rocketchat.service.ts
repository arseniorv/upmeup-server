import { CACHE_MANAGER, HttpException, HttpStatus, Inject, Injectable, Logger, Scope } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { randomBytes } from "crypto";
import { ChatCredentials, User } from "src/users/models/user";
import { Cache } from 'cache-manager';
import { OnEvent } from "@nestjs/event-emitter";
import { UserDeletedEvent } from "src/events/events";

const CREATE_USER_URL = "/api/v1/users.create";
const LOGIN_URL = "/api/v1/login";
const INSTANT_MESSAGES_URL = "/api/v1/im.list";
const CHAT_COUNTERS_URL = "/api/v1/im.counters";
const USER_INFO_URL = "/api/v1/users.info";
const UPDATE_USER_URL = "/api/v1/users.update";
const CREATE_IM_URL = "/api/v1/im.create";
const DELETE_USER_URL = "/api/v1/users.delete";

const ROCKETCHAT_LOGIN_DURATION_IN_DAYS = 90;


@Injectable({ scope: Scope.DEFAULT })
export class RocketChatService {
    private host: string;
    private username: string;
    private password: string;

    constructor(
        private configService: ConfigService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache
    ) {
        this.host = configService.get('ROCKETCHAT_ENDPOINT');
        this.username = configService.get('ROCKETCHAT_ADMIN_USERNAME');
        this.password = configService.get('ROCKETCHAT_ADMIN_PASSWORD');
    }

    async createUser(user: User) {
        const adminCredentials = await this.adminLogin()
        let username = this.generateUsername(user);
        if (await this.usernameExists(username)) {
            username += `.${randomBytes(2).toString('hex')}`;
        }
        const result = await fetch(`${this.host}${CREATE_USER_URL}`, {
            method: 'POST',
            body: JSON.stringify({
                email: user.email,
                name: user.type === 'candidate' ? `${user.name} ${user.surname}` : user.name,
                password: user.chatPassword,
                username
            }),
            headers: {
                "Content-type": "application/json",
                "X-Auth-Token": adminCredentials.chatAuthToken,
                "X-User-Id": adminCredentials.chatUserId
            }
        });
        Logger.debug('Rocket.Chat response on user creation');
        Logger.debug(result);
        if (result.ok) {
            return this.login(user.email, user.chatPassword);
        }
        Logger.error(`Failed to create rocketChat User for user: ${user.email}, ${username} : ${user.name} ${user.surname}`);
        throw new HttpException('INTERNAL SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    async updateUser(user: User): Promise<boolean> {
        const userInfo = await this.getUserInfo(user.chatUserId);
        if (!userInfo || !userInfo.username)
            return false;
        const { chatUserId, chatAuthToken } = await this.adminLogin();
        if (chatUserId && chatAuthToken) {
            let username = this.generateUsername(user);
            const existingUser = await this.usernameExists(username);
            if (existingUser && existingUser != user.chatUserId) {
                username += `.${randomBytes(2).toString('hex')}`;
            }
            const result = await fetch(`${this.host}${UPDATE_USER_URL}`, {
                method: 'POST',
                body: JSON.stringify({
                    userId: user.chatUserId,
                    data: {
                        username,
                        name: user.type === 'candidate' ? `${user.name} ${user.surname}` : user.name,
                        email: user.email
                    }
                }),
                headers: {
                    "Content-type": "application/json",
                    "X-Auth-Token": chatAuthToken,
                    "X-User-Id": chatUserId
                }
            });
            return result.ok;
        }
        return false;
    }

    @OnEvent('user.deleted')
    async handleUserDeletedEvent(event: UserDeletedEvent){
        this.deleteUser(event.user)
    }

    async deleteUser(user: User): Promise<boolean> {
        const userInfo = await this.getUserInfo(user.chatUserId);
        if (!userInfo || !userInfo.username)
            return false;
        const { chatUserId, chatAuthToken } = await this.adminLogin();

        if (chatUserId && chatAuthToken){
            const result = await fetch(`${this.host}${DELETE_USER_URL}`, {
                method: 'POST',
                body: JSON.stringify({
                    userId: user.chatUserId,
                    confirmRelinquish: true
                }),
                headers: {
                    "X-Auth-Token": chatAuthToken,
                    "X-User-Id": chatUserId,
                    "Content-type": "application/json"
                }
            }).then(rawResponse => rawResponse.json())
              .catch(error => {
                  console.log(error);
              })
              .then(response => {
                  console.log(response);
                  console.log("Deleting rocketchat user ", userInfo._id, "-", userInfo.name)
              })  
        }
        
        return
    }

    async getChatCredentials(chatUserId: string, password: string): Promise<ChatCredentials> {
        const userInfo = await this.getUserInfo(chatUserId);
        if (userInfo) {
            return this.login(userInfo.username, password)
        }
    }

    async createConversation(chatUserId: string, password: string, chatRecipientId: string): Promise<string> {
        const userInfo = await this.getChatCredentials(chatUserId, password);
        const recipientInfo = await this.getUserInfo(chatRecipientId)
        const result = await fetch(`${this.host}${CREATE_IM_URL}`, {
            method: 'POST',
            body: JSON.stringify({ username: recipientInfo.username }),
            headers: {
                "Content-type": "application/json",
                "X-Auth-Token": userInfo.chatAuthToken,
                "X-User-Id": userInfo.chatUserId
            }
        });
        if (result.ok) {
            return (await result.json()).room.rid;
        }
        return;
    }

    async getChats(userId: string, authToken: string) {
        const result = await fetch(`${this.host}${INSTANT_MESSAGES_URL}`, {
            headers: {
                "X-Auth-Token": authToken,
                "X-User-Id": userId
            }
        })
        if (!result.ok)
            return [];
        const chats = await result.json();
        return chats.ims;
    }

    async getChatUnreadCounter(roomId: string, userId: string, authToken: string) {
        const urlWithParams = `${this.host}${CHAT_COUNTERS_URL}?${new URLSearchParams({ roomId }).toString()}`
        const result = await fetch(urlWithParams, {
            headers: {
                "X-Auth-Token": authToken,
                "X-User-Id": userId
            }
        })
        if (!result.ok)
            return [];
        const counters = await result.json();
        return counters.unreads === null ? counters.msgs : counters.unreads; // mandatory since rocketchat might return null the first time a user access the room
    }

    private async login(email: string, password: string): Promise<ChatCredentials> {
        let chatCredentials: ChatCredentials = await this.cacheManager.get(`${email}-chatCredentials`);

        if (!chatCredentials) {
            const result = await fetch(`${this.host}${LOGIN_URL}`, {
                method: 'POST',
                body: JSON.stringify({ user: email, password }),
                headers: {
                    "Content-type": "application/json"
                }
            });

            if (result.ok) {
                const { data } = await result.json()
                const chatCredentials = {
                    chatUserId: data.userId,
                    chatAuthToken: data.authToken
                }
                this.cacheManager.set(`${email}-chatCredentials`, chatCredentials, {
                    ttl: 60 * 60 * 24 * ROCKETCHAT_LOGIN_DURATION_IN_DAYS
                })
                return chatCredentials
            }
        } else {
            return chatCredentials
        }
    }

    private async usernameExists(username: string): Promise<string> {
        const adminCredentials = await this.adminLogin();

        const result = await fetch(`${this.host}${USER_INFO_URL}?username=${username}`, {
            headers: {
                "Content-type": "application/json",
                "X-Auth-Token": adminCredentials.chatAuthToken,
                "X-User-Id": adminCredentials.chatUserId
            }
        })
        return result.ok ? (await result.json()).user._id : undefined;
    }

    private async adminLogin(): Promise<ChatCredentials> {
        const adminCredentials = await this.login(this.username, this.password);
        if (!adminCredentials) {
            Logger.error('Failed admin login for rocket.chat integration. Please check configured credentials are correct');
            throw new HttpException('INTERNAL SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return adminCredentials;
    }

    private async getUserInfo(chatUserId: string, adminChatUserId: string = undefined, adminChatAuthToken: string = undefined) {
        const adminCredentials = (adminChatAuthToken && adminChatUserId) ? { chatAuthToken: adminChatAuthToken, chatUserId: adminChatUserId } : await this.adminLogin();
        const result = await fetch(`${this.host}${USER_INFO_URL}?userId=${chatUserId}`, {
            headers: {
                "Content-type": "application/json",
                "X-Auth-Token": adminCredentials.chatAuthToken,
                "X-User-Id": adminCredentials.chatUserId
            }
        })
        if (result.ok) {
            return (await result.json()).user
        }
    }

    private generateUsername(user: User) {
        return `${user.name.toLowerCase().trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[^a-zA-Z0-9]+/g, '').replace(/ +/g, '.')}${user.type === 'candidate' ? '.' : ''}${user.type == 'candidate' ? user.surname.toLowerCase().trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/\.+/g, '').replace(/ +/g, '.') : ''}`;
    }
}