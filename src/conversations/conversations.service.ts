import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as nodemailer from 'nodemailer';
import * as nunjucks from 'nunjucks';
import { MessageNotification } from 'src/common/types/messageNotification';
import { UsersService } from 'src/users/users.service';
import { Cache } from 'cache-manager';

const MINUTES_BETWEEN_MESSAGES = 5;

@Injectable()
export class ConversationsService {

    constructor(
        private readonly userService: UsersService,
        private readonly configService: ConfigService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache
    ) { }

    async sendMessageNotification(messageNotification: MessageNotification) {
        Logger.debug(`Recived message notification ${messageNotification.chatUserId} -> ${messageNotification.chatRoomId}: ${messageNotification.message}`);
        const sender = await this.userService.getUserByChatUserId(messageNotification.chatUserId);
        var avatar
        var jobPosition
        if (!sender || !sender.rooms)
            return false;
        const room = sender.rooms.find(room => room.roomId === messageNotification.chatRoomId);
        if (!room || !room.userId)
            return false;
        const recipient = await this.userService.getUserById(room.userId.toString());
        if (!recipient)
            return false;
        if((await this.cacheManager.get(`${recipient._id}${sender._id}`))) {
            Logger.debug(`Not sending message notification for ${recipient._id}: ${recipient.name} as last notification was less than 5 minutes ago`);
            return true;
        }
        const mailerTransport = nodemailer.createTransport({
            pool: true,
            host: this.configService.get('MAIL_SMTP_SERVER'),
            port: this.configService.get('MAIL_SMTP_PORT') ? this.configService.get('MAIL_SMTP_PORT') : 465,
            secure: true, // use TLS
            auth: {
                user: this.configService.get('MAIL_SMTP_USER'),
                pass: this.configService.get('MAIL_SMTP_PASSWORD'),
            },
        });

        if(sender.avatarB64){
            avatar = sender.avatarB64;
        }
        else {
            avatar = "https://factodev.upmeup.es/assets/avatar.svg"
        }

        if(sender.jobPosition !== '-'){
            jobPosition = sender.jobPosition
        }
        else {
            jobPosition = '';
        }

        nunjucks.configure({ autoescape: true });
        const message = {
            from: "hola@upmeup.es",
            to: recipient.email,
            subject: "Mensaje recibido",
            text: nunjucks.render(`src/templates/message/es.txt`, { sender_name: sender.name, sender_avatar: avatar, sender_jobPosition: jobPosition}),
            html: nunjucks.render(`src/templates/message/es.html`, { sender_name: sender.name, sender_avatar: avatar, sender_jobPosition: jobPosition })

        };
        const result = await mailerTransport.sendMail(message);
        await this.cacheManager.set(`${recipient._id}${sender._id}`, Date.now(), 60*MINUTES_BETWEEN_MESSAGES);
        return true;
    }
}
