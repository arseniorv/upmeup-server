import { CacheModule, Module } from '@nestjs/common';
import { ConversationResolver } from './conversations.resolver';
import { UsersModule } from 'src/users/users.module';
import { RocketChatService } from 'src/common/rocketchat/rocketchat.service';
import { ConversationsService } from './conversations.service';

@Module({
  imports: [
    UsersModule,
    CacheModule.register()
  ],
  providers: [ConversationResolver, RocketChatService, ConversationsService],
})
export class ConversationsModule {}