import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType({ description: "MessageModel"})
export class Message {
    @Field()
    text: string;

    @Field()
    timeSent: string;
}