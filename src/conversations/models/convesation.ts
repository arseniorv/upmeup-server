import { Field, ID, ObjectType } from "@nestjs/graphql";
import { User } from "src/users/models/user";
import { Message } from "./message";

@ObjectType({ description: 'ConversationModel' })
export class Conversation {
    @Field(() => ID)
    _id: string;

    @Field()
    unreadsCount: number;

    @Field()
    messageCount: number;

    @Field(() => User)
    recipient: User;

    @Field(() => Message, { nullable: true })
    lastMessage: Message;
}


