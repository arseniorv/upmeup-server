Cuando una puerta se cierra, otras se abren.

Primero de todo queríamos agradecerte sinceramente por haberte postulado para la oferta laboral {{offer_title}} de {{company_name}}. Valoramos mucho el tiempo y el esfuerzo que has puesto en tu solicitud.

Después de revisar cuidadosamente todas las candidaturas, se ha tomado la decisión de no avanzar con la tuya en esta ocasión.

Te alentamos a que sigas buscando oportunidades que se alineen con tu perfil y experiencia y te agradecemos nuevamente por haberte apuntado a esta oferta laboral.

¡Mucho éxito en tu búsqueda de empleo en UpmeUp!