import { Injectable, Logger } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ProjectsService } from "src/projects/projects.service";
import { UsersService } from "src/users/users.service";
import { Match, MatchDocument, MatchScore } from "./models/match";

const CANDIDATE_MATCHWEIGHTS_CITY = {
    CONTENT: 0.25,
    COMPETENCIES: 0.25,
    SOFTSKILLS: 0.2,
    SECTOR: 0.15,
    CITY: 0.15
  }

  const CANDIDATE_MATCHWEIGHTS_NOCITY = {
    CONTENT: 0.3,
    COMPETENCIES: 0.3,
    SOFTSKILLS: 0.2,
    SECTOR: 0.2
  }

  const ENTITY_MATCHWEIGHTS_CITY = {
    CONTENT: 0.3,
    SOFTSKILLS: 0.3,
    SECTOR: 0.2,
    CITY: 0.2
  }

  const ENTITY_MATCHWEIGHTS_NOCITY = {
    CONTENT: 0.4,
    SOFTSKILLS: 0.3,
    SECTOR: 0.3
  }

@Injectable()
export class MatchService {

    constructor(@InjectModel(Match.name) private mModel: Model<MatchDocument>,
                private uService: UsersService,
                private pService: ProjectsService){}


    async getMatches(userID: string){
        return this.mModel.find({userID: userID});
    }

    async getMatch(userID: string, externalID: string){
        return this.mModel.find({userID: userID, externalID: externalID});
    }

    async calculateMatchByUser(userID: string): Promise<any>{
        const matches = await this.mModel.find({userID: userID});
        if (matches){
            for (let match of matches)
            await this.mModel.findByIdAndDelete(match._id);
        }

        const userInfo =  await this.uService.getUserById(userID);
        const userCompets = userInfo.competencies;
        const userSoftSkills = userInfo.softSkills;
        const userSectors = userInfo.sector;
        const userCity = userInfo.city;
        const userType = userInfo.type;

        const projects = await this.pService.getAllProjects();
        
        const matchProjects = await this.pService.filterByContent(userInfo);
        
        var userMatches: any[] = [];

        if(projects.length > 0){

            for (let project of projects){
                const matchScore = new MatchScore;
                
                const matchingCompetencies = userCompets.filter((c) => JSON.stringify(project.competencies).includes(JSON.stringify(c)));
                const matchingSoftSkills = userSoftSkills.filter((c) => JSON.stringify(project.softSkills).includes(JSON.stringify(c)));
                const matchingSectors = userSectors.filter((c) => JSON.stringify(project.sector).includes(JSON.stringify(c)));
    
                matchScore.competencies = project.competencies.length > 0 ? (matchingCompetencies.length / project.competencies.length) + 0.5 : 0.5; 
                matchScore.softSkills = project.softSkills.length > 0 ? matchingSoftSkills.length / project.softSkills.length : 0; 
                matchScore.sectors = project.sector.length > 0 ? matchingSectors.length / project.sector.length : 0; 
                if (project.city){
                    matchScore.city = project.city === userCity ? 1 : 0;
                }
                else {
                    matchScore.city = 0;
                }
    
                const matchProject = matchProjects.find(p => p._id.equals(project._id));
    
                if (matchProject) {
                    project._doc.score = matchProject._doc.score;
                }
    
                matchScore.content = project._doc.score && project.maxMatchScore ? (project._doc.score / project.maxMatchScore) + 0.5 : 0.5;
    
                //console.log("[calculateMatchByUser] Match score total: ", matc)
    
                if (userType === 'candidate'){
                    Logger.debug(`matchScore object before assingment:  ${matchScore}`);
                    if (project.city){
                        matchScore.total = (matchScore.competencies * CANDIDATE_MATCHWEIGHTS_CITY.COMPETENCIES
                        + matchScore.content * CANDIDATE_MATCHWEIGHTS_CITY.CONTENT
                        + matchScore.softSkills * CANDIDATE_MATCHWEIGHTS_CITY.SOFTSKILLS
                        + matchScore.sectors * CANDIDATE_MATCHWEIGHTS_CITY.SECTOR
                        + matchScore.city * CANDIDATE_MATCHWEIGHTS_CITY.CITY
                        ) * 100
                    }
                    else {
                        matchScore.total = (matchScore.competencies * CANDIDATE_MATCHWEIGHTS_NOCITY.COMPETENCIES
                            + matchScore.content * CANDIDATE_MATCHWEIGHTS_NOCITY.CONTENT
                            + matchScore.softSkills * CANDIDATE_MATCHWEIGHTS_NOCITY.SOFTSKILLS
                            + matchScore.sectors * CANDIDATE_MATCHWEIGHTS_NOCITY.SECTOR
                            ) * 100
                    }
                    Logger.debug(`matchScore object after assingment: ${matchScore}`);
       
                }
                else if (userType === 'company'){
                    if (project.city){
                        matchScore.total = (matchScore.content * ENTITY_MATCHWEIGHTS_CITY.CONTENT
                        + matchScore.softSkills * ENTITY_MATCHWEIGHTS_CITY.SOFTSKILLS
                        + matchScore.sectors * ENTITY_MATCHWEIGHTS_CITY.SECTOR
                        + matchScore.city * ENTITY_MATCHWEIGHTS_CITY.CITY
                        ) * 100
                    }
                    else {
                        matchScore.total = (matchScore.content * ENTITY_MATCHWEIGHTS_NOCITY.CONTENT
                            + matchScore.softSkills * ENTITY_MATCHWEIGHTS_NOCITY.SOFTSKILLS
                            + matchScore.sectors * ENTITY_MATCHWEIGHTS_NOCITY.SECTOR
                            ) * 100
                    }
                }
                var match = new Match();
                match.userID = userID;
                match.externalID = project._id;
                match.userType = userType;
                match.matchScore = matchScore;
    
                const createdItem = new this.mModel(match);
                const createdMatch = await createdItem.save();
                
                userMatches.push({'uId': userID, 'eId': project._id, 'match': matchScore.total} )
                match.calculatedMatches = userMatches
    
            }
            
            return match
        } else {
            var match = new Match();
            Logger.debug(`No projects to calculate match, sending empty array`);
            match.calculatedMatches = []
            return match
        }
        
    }

    async calculateMatchByProject (projectID: string){
        const matches = await this.mModel.find({externalID: projectID});
        if (matches){
            for (let match of matches)
            await this.mModel.findByIdAndDelete(match._id);
        }

        const projectInfo = await this.pService.getProject(projectID);
        const projectCompets = projectInfo.competencies;
        const projectSoftSkills = projectInfo.softSkills;
        const projectSectors = projectInfo.sector;
        const projectCity = projectInfo.city;
        const projectMaxScore : any = projectInfo.maxMatchScore;

        const users = await this.uService.getUsersList();

        var projectMatches: any[] = [];

        for (let user of users){
            const matchScore = new MatchScore;
            var userScore
            
            const matchingCompetencies = projectCompets.filter((c) => JSON.stringify(user.competencies).includes(JSON.stringify(c)));
            const matchingSoftSkills = projectSoftSkills.filter((c) => JSON.stringify(user.softSkills).includes(JSON.stringify(c)));
            const matchingSectors = projectSectors.filter((c) => JSON.stringify(user.sector).includes(JSON.stringify(c)));

            matchScore.competencies = user.competencies.length > 0 ? (matchingCompetencies.length / user.competencies.length) + 0.5 : 0.5; 
            matchScore.softSkills = user.softSkills.length > 0 ? matchingSoftSkills.length / user.softSkills.length : 0; 
            matchScore.sectors = user.sector.length > 0 ? matchingSectors.length / user.sector.length : 0; 
            if (projectCity){
                matchScore.city = user.city === projectCity ? 1 : 0;
            }
            else {
                matchScore.city = 0;
            }

            const matchUser = await this.pService.filterByContent(user, projectID);
            if (matchUser.length !== 0){
                for (let project of matchUser){
                   userScore = project._doc.score
                }
            } else {
                userScore = 0; 
            }

            matchScore.content = userScore && userScore!== 0 && projectMaxScore ? (userScore / projectMaxScore) + 0.5 : 0.5;


            if (user.type === 'candidate'){
                if (projectCity){
                    matchScore.total = (matchScore.competencies * CANDIDATE_MATCHWEIGHTS_CITY.COMPETENCIES
                    + matchScore.content * CANDIDATE_MATCHWEIGHTS_CITY.CONTENT
                    + matchScore.softSkills * CANDIDATE_MATCHWEIGHTS_CITY.SOFTSKILLS
                    + matchScore.sectors * CANDIDATE_MATCHWEIGHTS_CITY.SECTOR
                    + matchScore.city * CANDIDATE_MATCHWEIGHTS_CITY.CITY
                    ) * 100
                }
                else {
                    matchScore.total = (matchScore.competencies * CANDIDATE_MATCHWEIGHTS_NOCITY.COMPETENCIES
                        + matchScore.content * CANDIDATE_MATCHWEIGHTS_NOCITY.CONTENT
                        + matchScore.softSkills * CANDIDATE_MATCHWEIGHTS_NOCITY.SOFTSKILLS
                        + matchScore.sectors * CANDIDATE_MATCHWEIGHTS_NOCITY.SECTOR
                        ) * 100
                }
                
            }
            else if (user.type === 'company'){
                if (projectCity){
                    matchScore.total = (matchScore.content * ENTITY_MATCHWEIGHTS_CITY.CONTENT
                    + matchScore.softSkills * ENTITY_MATCHWEIGHTS_CITY.SOFTSKILLS
                    + matchScore.sectors * ENTITY_MATCHWEIGHTS_CITY.SECTOR
                    + matchScore.city * ENTITY_MATCHWEIGHTS_CITY.CITY
                    ) * 100
                }
                else {
                    matchScore.total = (matchScore.content * ENTITY_MATCHWEIGHTS_NOCITY.CONTENT
                        + matchScore.softSkills * ENTITY_MATCHWEIGHTS_NOCITY.SOFTSKILLS
                        + matchScore.sectors * ENTITY_MATCHWEIGHTS_NOCITY.SECTOR
                        ) * 100
                }
            }
            var match = new Match();
            match.userID = user._id.valueOf().toString();
            match.externalID = projectID;
            match.userType = user.type;
            match.matchScore = matchScore;

            const createdItem = new this.mModel(match);
            const createdMatch = await createdItem.save();
            
            
            projectMatches.push({'uId': user._id.valueOf().toString(), 'eId': projectID, 'match': matchScore.total} )
            match.calculatedMatches = projectMatches

        }
        return match

    }

    async deleteProjectMatches(projectID: string): Promise<Match> {
        const projectMatches = await this.mModel.find({externalID: projectID});
        var deletedMatches = [];
        for (let m of projectMatches){ 
            var match = new Match();
            const deletedMatch = await this.mModel.findByIdAndDelete(m._id)
            deletedMatches.push({'uId': deletedMatch.userID, 'eId': deletedMatch.externalID, 'match': deletedMatch.matchScore.total});
            match.calculatedMatches = deletedMatches
        }
        return match;
      }
}