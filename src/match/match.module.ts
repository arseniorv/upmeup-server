import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StorageService } from 'src/common/modules/s3storage/storageService';
import { ProjectModule } from 'src/projects/projects.module';
import { UsersModule } from 'src/users/users.module';
import { MatchResolver } from './match.resolver';
import { MatchService } from './match.service';
import { Match, MatchSchema } from './models/match';

@Module({
  imports: [
    UsersModule,
    ProjectModule,
    MongooseModule.forFeature([
      {
        name: Match.name,
        schema: MatchSchema,
      },
    ]),
  ],
  providers: [MatchService, MatchResolver, StorageService],
  exports: [MatchService]
})
export class MatchModule {}