import { Args, Mutation, Resolver, Query } from "@nestjs/graphql";

import { MatchService } from "./match.service";
import { Match } from "./models/match";



@Resolver(() => Match)
export class MatchResolver {

    constructor(public mService: MatchService){}

    @Query ((returns) => [Match])
    async getMatches(@Args('userID') userID: string){
        return await this.mService.getMatches(userID);
    }

    @Query ((returns) => [Match])
    async getMatch(
        @Args('userID') userID: string,
        @Args('externalID') externalID: string
    ){
        return await this.mService.getMatch(userID, externalID);
    }

    @Mutation(() => Match)
    async calculateMatchByUser(@Args('userID') userID: string){
        return this.mService.calculateMatchByUser(userID);
    }

    @Mutation(() => Match)
    async calculateMatchByProject(@Args('projectID') projectID: string){
        return this.mService.calculateMatchByProject(projectID);
    }

    @Mutation(() => Match)
    async deleteProjectMatches(@Args('projectID') projectID: string){
        return this.mService.deleteProjectMatches(projectID);
    }
}