import { Field, ID, ObjectType} from "@nestjs/graphql";
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, Types as MongooseTypes } from 'mongoose';

@ObjectType()
export class MatchScore {

  @Field(() => Number)
  @Prop()
  competencies: number;

  @Field(() => Number)
  @Prop()
  softSkills: number;

  @Field(() => Number)
  @Prop()
  content: number;

  @Field(() => Number, { nullable: true })
  @Prop()
  experience: number;

  @Field(() => Number)
  @Prop()
  sectors: number;

  @Field(() => Number, { nullable: true })
  @Prop()
  city: number;

  @Field(() => Number, { nullable: true })
  total: number;
}

@Schema()
@ObjectType()
export class Match {
    @Field(() => ID)
  _id: MongooseTypes.ObjectId;

  @Field(() => String)
  @Prop()
  userID: string;

  @Field(() => String)
  @Prop()
  externalID: string;

  @Field()
  @Prop()
  userType: string;

  @Field(() => MatchScore)
  @Prop()
  matchScore: MatchScore;

  @Field(() => [CalculatedMatches])
  calculatedMatches: CalculatedMatches[]
}

export type MatchDocument = Match & Document;
export const MatchSchema = SchemaFactory.createForClass(Match);

@ObjectType()
export class CalculatedMatches{

  @Field(() => String)
  uId: string;

  @Field(() => String)
  eId: string;

  @Field(() => Number)
  match: number;


}

