import { Field, InputType } from "@nestjs/graphql";
import { Prop } from "@nestjs/mongoose";
import mongoose from "mongoose";
import { ProjectStatus } from "src/common/types/projectStatus";
import { Competencies } from "src/competencies/models/competence";
import { Sector } from "src/sectors/models/sector";
import { Softskill } from "src/softskills/models/softskills";

export type ProjectsDocument = CreateProjectDto & mongoose.Document;

@InputType()
export class Members {
  @Field(() => String)
  @Prop()
  _id: String;

  @Field(() => String)
  @Prop()
  email: String;
}

@InputType({ description: 'Create new project' })
export class CreateProjectDto {
    @Field()
    userId: string;

    @Field()
    title: string;

    @Field()
    description: string;

    @Field(() => [Sector])
    sector: Sector[];

    @Field(() => [Competencies])
    competencies: Competencies[];

    @Field()
    city: string;

    @Field()
    user: string;

    @Field(() => [Softskill])
    softSkills: Softskill[];

    @Field(() => ProjectStatus, {nullable: true})
    status: ProjectStatus;

    @Field(() => Date)
    createdDate: Date;

    @Field(() => [String], {nullable: true})
    files: string[];

    @Field(() => [Members])
    admins: Members[];

}