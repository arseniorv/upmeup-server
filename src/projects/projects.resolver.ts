import { Args, Context, Mutation, Query, Resolver } from "@nestjs/graphql";
import { StorageService } from "src/common/modules/s3storage/storageService";
import { S3Url } from "src/common/types/s3Url";
import { CreateProjectDto } from "./dto/create-project.dto";
import { Project } from "./models/projects";
import { ProjectsService } from "./projects.service";
import { ProjectFilter } from "./inputs/project-filter.input";
import { UpdateProjectInput } from "./inputs/update-project.input";
import { HttpException, HttpStatus, UseGuards } from "@nestjs/common";
import { OfferStatus } from "src/common/types/offerStatus";
import { AuthGuard } from 'src/auth/auth.guard';

@Resolver(() => Project)
export class ProjectResolver {
    constructor(public pService: ProjectsService,
                private readonly storageService: StorageService
                ){}

    // @UseGuards(AuthGuard)
    @Query((returns) => Project)
    async getProject(@Args('id') id: string) {
        return this.pService.getProject(id);
    }

    // @UseGuards(AuthGuard)
    @Query(() => S3Url, { name: 'uploadProjectUrl'})
    async getPresignedUploadUrl(@Args('id') id: string, @Args('filename') filename: string) {
        return { presignedUrl: this.storageService.getPresignedUploadUrl(id, filename)}
    }

    @UseGuards(AuthGuard)
    @Query((returns) => [Project])
    async getAllProjects(
        @Args('userID', { nullable: true }) userID?: string,
        @Args('filter', { nullable: true }) filter?: ProjectFilter,
        @Args('limit', { nullable: true }) limit?: number,
        @Args('skip', { nullable: true }) skip?: number,
        ): Promise<Project[]> {
        return await this.pService.getAllProjects(userID, filter, limit, skip);
    }

    @Query((returns) => [Project], { name: 'getInterestProjects' })
    async getInterestProjects(@Args('userId') userID: string,) {
        return this.pService.getInterestProjects(userID);
    }

    @Query((returns) => S3Url, { name: 'downloadFile'})
    async getCVPresignedUrl(@Args('id') id: string, @Args('file') file: string) {
        return { presignedUrl: await this.storageService.getPresignedDownloadUrl(id, file) };
    }

    @UseGuards(AuthGuard)
    @Mutation(() => Project)
    async createProject(@Args('createProjectDto') createProjectDto: CreateProjectDto): Promise<Project> {
        return this.pService.createProject(createProjectDto);
    }

    @UseGuards(AuthGuard)
    @Mutation(() => Project, { name: 'editProject' })
    async editProject(
        @Args('id') _id: string,
        @Args('userId') userId: string,
        @Args({ name: 'projectInputs', type: () => UpdateProjectInput }) input: UpdateProjectInput,
        @Context() context
    ) {
        if (await this.pService.getProjectByIdAndOwnerId(_id, userId))
        return await this.pService.updateProject(_id, input);
        else
        throw new HttpException('Missing permissions to update this project', HttpStatus.FORBIDDEN)
    }

    @Mutation(() => Project, { name: 'addInterestedUser' })
    async addInterestedUser(
        @Args('projectId') projectId: string,
        @Args('interestedUserId') interestedUserId: string,
        @Context() context
    ) {
        return await this.pService.addInterestedUser(projectId, interestedUserId);
    }

    @Mutation(() => Project, { name: 'removeInterestedUser' })
    async removeInterestedUser(
        @Args('projectId') projectId: string,
        @Args('interestedUserId') interestedUserId: string,
        @Context() context
    ) {
        return await this.pService.removeInterestedUser(projectId, interestedUserId);
    }


    @Mutation(() => Project, { name: 'updateProjectState' })
    async updateProjectState(
        @Args('pId') pId: string,
        @Args({ name: 'state', type: () => OfferStatus }) state: OfferStatus
    ) {
        return await this.pService.updateProjectState(pId, state);
    }

    @Mutation(() => Project, { name: 'deleteProject' })
   async deleteProject(@Args('id') _id: string): Promise<Project> {
     return this.pService.deleteProject(_id);
   }
}