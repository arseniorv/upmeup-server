import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Project, ProjectSchema } from './models/projects';
import { ProjectResolver } from './projects.resolver';
import { ProjectsService } from './projects.service';
import { StorageService } from 'src/common/modules/s3storage/storageService';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    UsersModule,
    MongooseModule.forFeature([
      {
        name: Project.name,
        schema: ProjectSchema,
      },
    ])
  ],
  providers: [ProjectsService, ProjectResolver, StorageService],
  exports:[ProjectsService]
})
export class ProjectModule {}