import { Field, ID, ObjectType, registerEnumType } from "@nestjs/graphql";
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { ProjectStatus } from "src/common/types/projectStatus";
import { Competencies } from "src/competencies/models/competence";
import { Sector } from "src/sectors/models/sector";
import { Softskill } from "src/softskills/models/softskills";
import mongoose, { Document, Types as MongooseTypes } from 'mongoose';
import { User } from "src/users/models/user";
import { OfferStatus } from "src/common/types/offerStatus";

registerEnumType(ProjectStatus, { name: 'ProjectStatus'});

@Schema()
@ObjectType({description: 'ProjectModel'})
export class Project {
    @Field(() => ID)
    _id: MongooseTypes.ObjectId;

    @Field(() => String)
    @Prop()
    userId: string;

    @Field(() => String)
    @Prop()
    title: string;

    @Field(() => String)
    @Prop()
    description: string;

    @Field(() => [Sector])
    @Prop()
    sector: Sector[];

    @Field(() => [Competencies])
    @Prop()
    competencies: Competencies[];

    @Field(() => String, { nullable: true })
    @Prop()
    city: string;

    @Field(() => User)
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
    user: User;

    @Field(() => [Softskill])
    @Prop()
    softSkills: Softskill[];

    @Field(() => ProjectStatus, {nullable: true})
    @Prop()
    status: ProjectStatus;

    @Field(() => Date)
    @Prop()
    createdDate: Date;

    @Field(() => [String], {nullable: true})
    @Prop()
    files: string[];

    @Field(() => [InterestedUser], {nullable: true})
    @Prop()
    interestedUsers: InterestedUser[];

    @Field(() => Number)
    @Prop({})
    enrolled: number;

    @Field(() => OfferStatus, { nullable: true } )
    @Prop({ type: String, enum: OfferStatus })
    state: OfferStatus;

    @Field(() => [Member])
    @Prop()
    admins: Member[];

    @Field(() => Number)
    @Prop()
    maxMatchScore: Number;

}

export type ProjectDocument = Project & Document;
export const ProjectSchema = SchemaFactory.createForClass(Project);

ProjectSchema.index({ title: 'text', description: 'text', requirements: 'text'}, { default_language: 'spanish'})

@ObjectType()
export class InterestedUser {

  @Field(() => String)
  @Prop()
  _id: String;

  @Field(() => String)
  name: String;
}

@ObjectType()
export class Member {
  @Field(() => String)
  @Prop()
  _id: String;

  @Field(() => String)
  @Prop()
  email: String;
}