/* eslint-disable prettier/prettier */
import { Field, InputType } from '@nestjs/graphql';
import * as mongoose from 'mongoose';
import { ProjectStatus } from 'src/common/types/projectStatus';
import { Competencies } from 'src/competencies/models/competence';
import { Sector } from 'src/sectors/models/sector';
import { Softskill } from 'src/softskills/models/softskills';
import { Members } from '../dto/create-project.dto';

export type CompanyOfferDocument = UpdateProjectInput & mongoose.Document;

@InputType({ description: 'Update Project document' })
export class UpdateProjectInput {
    @Field({ nullable: true })
    title: string;
    
    @Field({ nullable: true })
    description: string;

    @Field(() => [Sector], {nullable: true})
    sector: Sector[];

    @Field(() => [Competencies], {nullable: true})
    competencies: Competencies[];

    @Field({ nullable: true })
    city: string;
  
    @Field(() => [Softskill], {nullable: true})
    softSkills: Softskill[];

    @Field(() => ProjectStatus, {nullable: true})
    status: ProjectStatus;

    @Field(() => [String], {nullable: true})
    files: string[];

    @Field(() => Number, { nullable: true })
    enrolled: number;

    @Field(() => [String], { nullable: true})
    interestedUsers: String[];

    @Field(() => [Members])
    admins: Members[];

}
