import { Field, InputType } from "@nestjs/graphql";
import { Sector } from "src/sectors/models/sector";

@InputType()
export class ProjectFilter {
    
    @Field(() => Date, { nullable: true })
    createdDate: Date

    @Field(() => [String], { nullable: true })
    city: string[];

    @Field(() => [String], { nullable: true })
    status: string[];
  
    @Field(() => [Sector], { nullable: true })
    sector: Sector[];

    @Field(() => String, { nullable: true })
    search: string;


}