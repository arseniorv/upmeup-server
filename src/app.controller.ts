import { Body, Controller, Get, HttpException, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { ConversationsService } from './conversations/conversations.service';
import { MessageNotification } from './common/types/messageNotification';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private readonly conversationsService: ConversationsService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('/message/notification')
  async sendMessageNotification(@Body() messageNotification: MessageNotification) {
    if(await this.conversationsService.sendMessageNotification(messageNotification)) 
      return 'Notification sent succssefully';
    throw new HttpException('Something went wrong', 500);
  }

}
