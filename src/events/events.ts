import { User } from "src/users/models/user";

export class UserDeletedEvent {
    constructor(public readonly user: User){}
}