import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

import { urlencoded, json } from 'express';
import { LoggerFactory } from './common/modules/LoggerFactory';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
    logger: LoggerFactory('upmeup-backend')
  });

  app.use(json({ limit: '1000mb' }));
  app.use(urlencoded({ extended: true, limit: '1000mb' }));

  //app.enableCors();
  app.enableCors({
    origin: '*',
    allowedHeaders: 'content-type, Accept, Authorization',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });
 
  await app.listen(3000, '0.0.0.0');

  Logger.log('', 'Start');
  Logger.log(`API RUNNING: ${await app.getUrl()}`, 'Start');
  Logger.log('', 'Start');
  
}
bootstrap();
