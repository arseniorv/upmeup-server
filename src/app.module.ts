import { CacheModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './common/modules/database/database.module';
import { GraphQLModule as GraphQL } from './common/modules/graphql/graphql.module';
import { CompanyOffersModule } from './company-offers/company-offers.module';
import { CompetenciesModule } from './competencies/competencies.module';
import { ConversationsModule } from './conversations/conversations.module';
import { ConversationsService } from './conversations/conversations.service';
import { MatchModule } from './match/match.module';
import { ProjectModule } from './projects/projects.module';
import { SectorsModule } from './sectors/sectors.module';
import { SoftskillsModule } from './softskills/softskills.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    ProjectModule,
    MatchModule,
    DatabaseModule,
    GraphQL,
    UsersModule,
    AuthModule,
    CompanyOffersModule,
    ConversationsModule,
    SoftskillsModule,
    SectorsModule,
    CompetenciesModule,
    CacheModule.register(),
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env'], 
      isGlobal: true
    }),
    EventEmitterModule.forRoot({
      delimiter: '.'
    })
  ],
  controllers: [AppController],
  providers: [AppService, ConversationsService],
})
export class AppModule {}
