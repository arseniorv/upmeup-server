import { Field, InputType } from "@nestjs/graphql";
import { RequiredExperience } from "src/common/types/requiredExpirience";

@InputType()
export class OfferFilter {
    
    @Field(() => String, { nullable: true })
    userId: string

    @Field(() => Date, { nullable: true })
    createdDate: Date

    @Field(() => [String], { nullable: true })
    city: string[];

    @Field(() => [String], { nullable: true })
    salaryRange: string[];
  
    @Field(() => [String], { nullable: true })
    remote: string[];
  
    @Field(() => [String], { nullable: true })
    contractType: string[];
  
    @Field(() => [String], { nullable: true })
    workHours: string[];

    @Field(() => String, { nullable: true })
    search: string;

    @Field(() => [RequiredExperience], { nullable: true })
    requiredExperience: RequiredExperience[];

}