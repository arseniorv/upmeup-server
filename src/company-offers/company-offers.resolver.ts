/* eslint-disable prettier/prettier */
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AuthGuard } from 'src/auth/auth.guard';
import { StorageService } from 'src/common/modules/s3storage/storageService';
import { CandidatureStatus } from 'src/common/types/candidatureStatus';
import { OfferStatus } from 'src/common/types/offerStatus';
import { S3Url } from 'src/common/types/s3Url';
import { CompanyOffersService } from './company-offers.service';
import { CreateOfferDto } from './dto/create-offer.dto';
import { OfferFilter } from './dto/inputs/offer-filter.input';
import { UpdateOfferInput } from './dto/inputs/update-offer.input';
import { Offer } from './models/company-offer';
import { Types } from 'mongoose';

@Resolver(() => Offer)
export class CompanyOffersResolver {
  constructor(private readonly coffService: CompanyOffersService, private readonly storageService: StorageService) { }

  @UseGuards(AuthGuard)
  @Query((returns) => [Offer])
  async getCompanyOffers(
    @Context() context,
    @Args('filter', { nullable: true }) filter?: OfferFilter,
    @Args('limit', { nullable: true }) limit?: number,
    @Args('skip', { nullable: true }) skip?: number): Promise<Offer[]> {
    let offers = await this.coffService.findAll(filter, limit, skip);
    for(let i=0;i<offers.length; i++) {
      if (!new Types.ObjectId(context.user._id).equals(offers[i].user._id)) {
        offers[i].candidates = offers[i].candidates.filter(candidate => candidate.user._id.equals(new Types.ObjectId(context.user._id)));
      }
    }
    return offers;
  }

  /**
   * Offer By ID
   */
  @UseGuards(AuthGuard)
  @Query((returns) => Offer)
  async getOffer(@Context() context, @Args('id') id: string) {
    const offer = await this.coffService.getOffer(id);
    if (!new Types.ObjectId(context.user._id).equals(offer.user._id)) {
      offer.candidates = offer.candidates.filter(candidate => candidate.user._id.equals(new Types.ObjectId(context.user._id)));
      offer.user.rooms = offer.user.rooms.filter(room => room.userId.equals(new Types.ObjectId(context.user._id)));
    }
    return offer;
  }

  @UseGuards(AuthGuard)
  @Query((returns) => [Offer], { name: 'myCandidatures' })
  async getLoggedUserCandidatures(@Context() context) {
    return this.coffService.getOffersByCandidate(context.user._id);
  }

  /**
   * Mutation: Create new offer resolver
   */
  @UseGuards(AuthGuard)
  @Mutation(() => Offer)
  async createOffer(@Args('createOfferDto') createOfferDto: CreateOfferDto): Promise<Offer> {
    return this.coffService.createOffer(createOfferDto);
  }

  @UseGuards(AuthGuard)
  @Query(() => S3Url, { name: 'uploadOfferUrl' })
  async getPresignedUploadUrl(@Args('id') id: string, @Args('filename') filename: string) {
    return { presignedUrl: this.storageService.getPresignedUploadUrl(id, filename) }
  }

  @Query((returns) => S3Url, { name: 'downloadFile' })
  @UseGuards(AuthGuard)
  async getCVPresignedUrl(@Args('id') id: string, @Args('file') file: string) {
    return { presignedUrl: await this.storageService.getPresignedDownloadUrl(id, file) };
  }

  /**
  * Mutation: Delete offer
  */
  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'deleteOffer' })
  async deleteOffer(@Args('id') _id: string): Promise<Offer> {
    return this.coffService.deleteOffer(_id);
  }

  /**
   * Mutation: Update Offer
   */
  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'updateOffer' })
  async updateOffer(
    @Args('id') _id: string,
    @Args({ name: 'offerInputs', type: () => UpdateOfferInput }) input: UpdateOfferInput,
    @Context() context
  ) {
    if (await this.coffService.getOfferByIdAndOwnerId(_id, context.user._id))
      return await this.coffService.updateOffer(_id, input);
    else
      throw new HttpException('Missing permissions to update this offer', HttpStatus.FORBIDDEN)
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'addCandidate' })
  async addCandidate(
    @Args('offerId') offerId: string,
    @Args('candidateId') candidateId: string,
    @Context() context
  ) {
    return await this.coffService.addCandidate(offerId, context.user._id);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'removeCandidate' })
  async removeCandidate(
    @Args('offerId') offerId: string,
    @Args('candidateId') candidateId: string,
    @Context() context
  ) {
    return await this.coffService.removeCandidate(offerId, context.user._id);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'updateCandidateStatus' })
  async updateCandidateStatus(
    @Args('offerId') offerId: string,
    @Args('candidateId') candidateId: string,
    @Args({ name: 'status', type: () => CandidatureStatus }) status: CandidatureStatus,
  ) {
    return await this.coffService.updateCandidateStatus(offerId, candidateId, status);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'updateOfferStatus' })
  async updateOfferStatus(
    @Args('offerId') offerId: string,
    @Args({ name: 'status', type: () => OfferStatus }) status: OfferStatus,
  ) {
    return await this.coffService.updateOfferStatus(offerId, status);
  }

}
