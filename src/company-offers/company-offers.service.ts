/* eslint-disable prettier/prettier */
import { BadRequestException, Inject, Injectable, Logger, Scope } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { OnEvent } from '@nestjs/event-emitter';
import { CONTEXT } from '@nestjs/graphql';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import * as nodemailer from 'nodemailer';
import * as nunjucks from 'nunjucks';
import { RocketChatService } from 'src/common/rocketchat/rocketchat.service';
import { CandidatureStatus } from 'src/common/types/candidatureStatus';
import { OfferStatus } from 'src/common/types/offerStatus';
import { RequiredExperience } from 'src/common/types/requiredExpirience';
import { UserType } from 'src/common/types/userType';
import { UserDeletedEvent } from 'src/events/events';
import { User } from 'src/users/models/user';
import { UsersService } from 'src/users/users.service';
import { CreateOfferDto } from './dto/create-offer.dto';
import { OfferFilter } from './dto/inputs/offer-filter.input';
import { UpdateOfferInput } from './dto/inputs/update-offer.input';
import { Offer, OfferDocument, OfferMatch } from './models/company-offer';

const MATCHWEIGHTS = {
  CONTENT: 0.3,
  COMPETENCIES: 0.3,
  SOFTSKILLS: 0.2,
  EXPERIENCIE: 0.2
}

@Injectable({ scope: Scope.REQUEST })
export class CompanyOffersService {
  constructor(
    @Inject(CONTEXT) private context: any,
    @InjectModel(Offer.name) private coffModel: Model<OfferDocument>,
    private configService: ConfigService,
    private chatService: RocketChatService,
    private readonly uService: UsersService
  ) { }

  async findAll(filter?: OfferFilter, limit?: number, skip?: number): Promise<Offer[]> {

    const formatedFilter: any = filter

    if (filter) {
      if (filter.createdDate)
        formatedFilter.createdDate = { $gt: filter.createdDate }
      if (filter.search) {
        formatedFilter.$text = { $search: filter.search }
        delete formatedFilter.search
      }
    }

    try {
      const loggedUser = await this.uService.getUserById(this.context.user._id);
      let matchFilter;
      let offers;

      //For companies there is no need to calculate the match 
      if (loggedUser.type === UserType.COMPANY) {
        const offers = await this.coffModel
          .find(formatedFilter)
          .sort({ createdDate: -1 })
          .skip(skip)
          .limit(limit)
          .populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }]);
          offers.forEach(offer => {
            offer.candidates = offer.candidates.filter(candidates => candidates.user?._id);
          });
        return offers;
      }

      //Since we need to use the text index both to filter and to calculate the match, if there is a filter present we need to retrieve the offers in two steps
      if (formatedFilter) {
        offers = (await this.coffModel
          .find(formatedFilter)
          .skip(skip)
          .limit(limit)
          .populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }])
          .sort({ createdDate: -1 })) as any;

        matchFilter = { $text: { $search: `${loggedUser.lastJobTasks} ${loggedUser.jobPosition}` }, _id: { $in: offers.map(offer => offer._id) }, active: true }
        const matchOffers = (await this.coffModel.find(matchFilter, { score: { $meta: "textScore" } })) as any;

        offers.forEach(offer => {
          const matchOffer = matchOffers.find(o => o._id.equals(offer._id));
          if (matchOffer) {
            offer._doc.score = matchOffer._doc.score;
          }
          offer.match = this.calculateMatch(offer, loggedUser).total;
        });

      } else {
        matchFilter = { $or: [{ active: true }, { $text: { $search: `${loggedUser.lastJobTasks} ${loggedUser.jobPosition}` }, active: true }] }
        offers = await this.coffModel
          .find(matchFilter, { score: { $meta: "textScore" } })
          .skip(skip)
          .limit(limit)
          .populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }])
          .sort({ createdDate: -1 }) as any;

        offers.forEach(offer => {
          offer.match = this.calculateMatch(offer, loggedUser).total;
          offer.candidates = offer.candidates.filter(candidates => candidates.user?._id);
        });
      }

      return offers;

    } catch (error) {
      Logger.error(error);
      throw new BadRequestException();
    }
  }

  /**
   * Offer by ID
   *  */
  async getOffer(_id: string): Promise<Offer> {
    const loggedUser = await this.uService.getUserById(this.context.user._id);
    if (loggedUser.type === UserType.COMPANY) {
      const offer = await this.coffModel.findById(_id).populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }]);
      offer.candidates = offer.candidates.filter(candidates => candidates.user?._id);
      return offer;
    }
    const matchFilter = { $text: { $search: `${loggedUser.lastJobTasks} ${loggedUser.jobPosition}` }, _id: _id }
    const matchOffers = (await this.coffModel.find(matchFilter, { score: { $meta: "textScore" } }).populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }])) as any;
    if (matchOffers.length == 0) {
      const offer = await this.coffModel.findById(_id).populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }]) as any;
      offer.candidates = offer.candidates.filter(candidates => candidates.user?._id);
      offer.match = this.calculateMatch(offer, loggedUser).total;
      return offer;
    }
    const offer = matchOffers[0];
    offer.candidates = offer.candidates.filter(candidates => candidates.user?._id);
    offer.match = this.calculateMatch(offer, loggedUser).total;
    return offer;
  }

  /**
 * Offer by ID and ownerId (usefull to check update permisions)
 *  */
  async getOfferByIdAndOwnerId(_id: string, ownerId: string): Promise<Offer> {
    return this.coffModel.findOne({ _id, userId: ownerId });
  }

  async getOffersByCandidate(candidateId: string) {
    Logger.debug(`Retrieving offers where ${candidateId} is a candidate`);
    return this.coffModel.find({ "candidates.user": candidateId }).populate([{ path: 'user' }, {
      path: 'candidates.user',
      model: User.name
    }])
  }

  /**
   * Create new Offer
   *  */
  async createOffer(createOfferDto: CreateOfferDto): Promise<Offer> {
    const createdItem = new this.coffModel(createOfferDto);
    createdItem.active = true;
    const createdOffer = await createdItem.save();
    Logger.debug(`Created new offer: ${JSON.stringify(createOfferDto)}`);
    const maxScoreOffer = await this.coffModel.findOne({ $or: [{ $text: { $search: `${createdOffer.title} ${createdOffer.description} ${createdOffer.requirements}` }, _id: createdOffer._id }, { _id: createdOffer._id }] }, { score: { $meta: "textScore" } }) as any;
    if (maxScoreOffer._doc.score)
      await this.coffModel.findByIdAndUpdate(createdOffer._id, { maxMatchScore: maxScoreOffer._doc.score });
    else
      Logger.warn(`Missing max match score for new offer: ${createOfferDto.title}`);

    return createdOffer;
  }

  /**
 * Delete Offer
 *  */
  async deleteOffer(_id: string): Promise<Offer> {
    const deletedOffer = await this.coffModel.findByIdAndDelete(_id)

    return deletedOffer;
  }

  /**
   * Update Offer oject
   * @param _id 
   * @param updateOfferInput 
   * @returns 
   */
  async updateOffer(_id: string, updateOfferInput: UpdateOfferInput) {
    const updatedOffer = await this.coffModel.findByIdAndUpdate(_id, updateOfferInput, { new: true }).populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }]);
    Logger.debug(`Updated offer: ${JSON.stringify(updateOfferInput)}`);
    const maxScoreOffer = await this.coffModel.findOne({ $or: [{ $text: { $search: `${updatedOffer.title} ${updatedOffer.description} ${updatedOffer.requirements}` }, _id: updatedOffer._id }, { _id: updatedOffer._id }] }, { score: { $meta: "textScore" } }) as any;
    if (maxScoreOffer._doc.score)
      await this.coffModel.findByIdAndUpdate(updatedOffer._id, { maxMatchScore: maxScoreOffer._doc.score });
    else
      Logger.warn(`Missing max match score for updated offer: '${updateOfferInput.title}'`);
    return updatedOffer;
  }

  async addCandidate(_id: string, candidateId: string) {

    const candidate = await this.uService.getUserById(candidateId);

    const search = `${candidate.lastJobTasks} ${candidate.jobPosition}`;

    const offer = await this.coffModel.findOne({ $or: [{ $text: { $search: search }, _id: _id }, { _id: _id }] }, { score: { $meta: "textScore" } }).populate('user') as any;

    const match = this.calculateMatch(offer, candidate)

    const candidates = offer.candidates as Array<any>;
    const candidateIndex = candidates.findIndex(c => c.user === candidateId);

    const offerInput = new UpdateOfferInput();
    if (candidateIndex !== -1) {
      candidates[candidateIndex] = { user: candidateId, match: match }
    } else {
      candidates.push({ user: candidateId, match: match });
      offerInput.enrolled = candidates.length;
    }

    offerInput.candidates = candidates;
    Logger.debug(`Adding candidate ${candidate._id} to offer "${offer._id} - ${offer.title}"`);
    return await this.coffModel.findByIdAndUpdate(_id, offerInput, { new: true }).populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }]);
  }

  async removeCandidate(_id: string, candidateId: string) {
    const candidate = await this.uService.getUserById(candidateId);
    const search = `${candidate.lastJobTasks} ${candidate.jobPosition}`;
    let offer = await this.coffModel.findOne({ $or: [{ $text: { $search: search }, _id: _id }, { _id: _id }] }, { score: { $meta: "textScore" } }).populate('user') as any;
    const candidates = offer.candidates as Array<any>;
    const candidateIndex = candidates.findIndex(c => c.user.toString() === new Types.ObjectId(candidateId).toString());

    
    // update offer.candidates[] array
    if (candidateIndex !== -1) {
      // remove if present
      const offerInput = new UpdateOfferInput();
      candidates.splice(candidateIndex, 1);
      offerInput.candidates = candidates;
      // update
      offer = await this.coffModel.findByIdAndUpdate(_id, offerInput, { new: true }).populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }]);
      Logger.debug(`Removing candidate ${candidate._id} from offer "${offer._id} - ${offer.title}"`);
    } else {
      Logger.log (`removing candidate :: user ${candidate._id} NOT present in project : ${offer._id}`)
    }
    return offer;
  }

  async updateCandidateStatus(offerId: string, candidateId: string, status: CandidatureStatus, statusUpDate?: Date): Promise<Offer> {
    const offer = await this.coffModel.findOne({"_id": offerId}).populate([{path: 'user'}, {path: 'candidates.user'}]);
    
    for (let c of offer.candidates){
      statusUpDate = new Date();
      if (c.user.toString() === candidateId){
        c.status = status;
        c.statusUpDate = statusUpDate;
      }
    }
    await this.coffModel.findOneAndUpdate({_id: offerId}, {candidates: offer.candidates})
    Logger.log(`Status of candidate ${candidateId} updated to ${status} by ${this.context.user._id} on ${statusUpDate}`)

    const candidate = await this.uService.getUserById(candidateId);

    if (status === CandidatureStatus.PRESELECTED) {
      this.uService.createChatRoomIfNotExists(this.context.user._id, candidate._id.toString());
    }

    if (status === CandidatureStatus.REJECTED) {
      const lang = candidate.preferences?.language ? candidate.preferences.language : 'es';
      const mailerTransport = nodemailer.createTransport({
        pool: true,
        host: this.configService.get('MAIL_SMTP_SERVER'),
        port: this.configService.get('MAIL_SMTP_PORT') ? this.configService.get('MAIL_SMTP_PORT') : 465,
        secure: true, // use TLS
        auth: {
          user: this.configService.get('MAIL_SMTP_USER'),
          pass: this.configService.get('MAIL_SMTP_PASSWORD'),
        },
      });

      nunjucks.configure({ autoescape: true });
      const message = {
        from: "hola@upmeup.es",
        to: candidate.email,
        subject: "Esta vez no ha podido ser",
        // text: nunjucks.render(`src/templates/rejected/${lang}.txt`, { offer_title: offer.title, company_name: offer.user.name }),
        // html: nunjucks.render(`src/templates/rejected/${lang}.html`, { offer_title: offer.title, company_name: offer.user.name })
        text: nunjucks.render(`src/templates/rejected/es.txt`, { offer_title: offer.title, company_name: offer.user.name }),
        html: nunjucks.render(`src/templates/rejected/es.html`, { offer_title: offer.title, company_name: offer.user.name })
      };

      await mailerTransport.sendMail(message);
    }

    return offer;
  }

  async updateOfferStatus(offerId: string, status: OfferStatus): Promise<Offer> {
    await this.coffModel.updateOne(
      { "_id": offerId },
      { "$set": { "status": status } }
    );
    return this.coffModel.findOne({ "_id": offerId });
  }


  calculateMatch(offer: any, candidate: User): OfferMatch {

    const candidateCompetencies = candidate.competencies.map(competency => competency._id);
    const candidateSoftSkills = candidate.softSkills.map(softskill => softskill._id);

    const offerCompetencies = new Set(offer.competencies.map(competency => competency._id));
    const offerSoftSkills = new Set(offer.user.softSkills.map(softskill => softskill._id));

    // Intersection between the arrays to find matching Ids https://stackoverflow.com/questions/1885557/simplest-code-for-array-intersection-in-javascript
    const matchingCompetencies = [... new Set(candidateCompetencies)].filter((c) => offerCompetencies.has(c));
    const matchingSoftSkills = [... new Set(candidateSoftSkills)].filter(s => offerSoftSkills.has(s));

    const match = new OfferMatch();
    Logger.warn(`Match between offer ${offer._id} and candidate ${candidate._id} is missing content score`);    
    match.competencies = offerCompetencies.size > 0 ? (matchingCompetencies.length / offerCompetencies.size) + 0.5 : 0.5; 
    match.softSkills = offerSoftSkills.size > 0 ? matchingSoftSkills.length / offerSoftSkills.size : 0;
    match.content = offer._doc.score && offer.maxMatchScore ? (offer._doc.score / offer.maxMatchScore) + 0.5 : 0.5;
    match.experience = this.calculateExperienceScore(offer.requiredExperience, +candidate.experience)
    match.total = (match.competencies * MATCHWEIGHTS.COMPETENCIES
      + match.content * MATCHWEIGHTS.CONTENT
      + match.experience * MATCHWEIGHTS.EXPERIENCIE
      + match.softSkills * MATCHWEIGHTS.SOFTSKILLS) * 100
    Logger.log(offer.title, offer._doc.score, offer.maxMatchScore, JSON.stringify(match));
    return match;
  }

  calculateExperienceScore(requiredExperience: RequiredExperience, candidateExperience: Number) {
    let min: number, max: number;

    if (requiredExperience === RequiredExperience.JUNIOR) {
      min = 0;
      max = 2;
    } else if (requiredExperience === RequiredExperience.MID) {
      min = 2
      max = 6
    } else {
      min = 7
      max = Number.POSITIVE_INFINITY;
    }

    if (+candidateExperience < min) {
      const experienceScore = 1 - (min - (+candidateExperience)) * 0.1;
      return experienceScore > 0 ? experienceScore : 0;
    }
    if (+candidateExperience > max) {
      const experienceScore = 1 - (+candidateExperience - max) * 0.1;
      return experienceScore > 0 ? experienceScore : 0;
    }
    return 1;
  }

  @OnEvent('user.deleted')
    async handleUserDeletedEvent(event: UserDeletedEvent){
      let userId : string = event.user._id.toString();
      // Delete user offers
      let offersToDelete = (await this.coffModel.find({userId: userId})).map(async (offer)=>{
        const deletedOffer = await this.coffModel.findOneAndDelete(offer._id)
        console.log('deleting offer...', deletedOffer._id, "-", deletedOffer.title )
      });
      
      // Delete user candidatures
      let offers = (await this.coffModel.find()).map(async (offer) => {
        const offerId = offer._id;
        const candidates = offer.candidates as Array<any>;
        const candidateIndex = candidates.findIndex(c => c.user.toString() === new Types.ObjectId(userId).toString());
        const user_present = (candidateIndex !== -1)
        //TODO: código duplicado en removeCandidate(); revisar si se puede reutilizar
        if (user_present) {
          // remove if present
          const offerInput = new UpdateOfferInput();
          candidates.splice(candidateIndex, 1);
          offerInput.candidates = candidates;
          // update
          offer = await this.coffModel.findByIdAndUpdate(offerId, offerInput, { new: true }).populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }]);
          console.log(`Removing candidate ${userId} from offer "${offerId} - ${offer.title}"`);
        }
      })

      return
    }


}
