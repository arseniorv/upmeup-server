import { CacheModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from 'src/users/users.module';
import { CompanyOffersResolver } from './company-offers.resolver';
import { CompanyOffersService } from './company-offers.service';
import { Offer, OfferSchema } from './models/company-offer';
import { StorageService } from 'src/common/modules/s3storage/storageService';
import { RocketChatService } from 'src/common/rocketchat/rocketchat.service';

@Module({
  imports: [
    UsersModule,
    MongooseModule.forFeature([
      {
        name: Offer.name,
        schema: OfferSchema,
      },
    ]),
    CacheModule.register()
  ],
  providers: [CompanyOffersService, CompanyOffersResolver, StorageService, RocketChatService],
})
export class CompanyOffersModule {}
