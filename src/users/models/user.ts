/* eslint-disable prettier/prettier */
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Mongoose, Types as MongooseTypes, ObjectId } from 'mongoose';
import { CandidatureStatus } from 'src/common/types/candidatureStatus';
import { Competencies } from 'src/competencies/models/competence';
import { Sector } from 'src/sectors/models/sector';
import { Softskill } from 'src/softskills/models/softskills';

@ObjectType()
export class Preferences {

  @Field(() => String, { nullable: true })
  @Prop()
  language: String;

}

@ObjectType()
export class Candidatures { // TODO - Remove this from all codebase, it is not necessary anymore (careful not to remove the offer equivalent)

  @Field(() => String, { nullable: true })
  offerID: String;

  @Field(() => CandidatureStatus, { nullable: true })
  status: CandidatureStatus;

  @Field(() => String, { nullable: true })
  offerTitle: String;

}

@ObjectType()
export class Room {
  @Field(() => String)
  @Prop()
  userId: MongooseTypes.ObjectId;

  @Field(() => String)
  @Prop()
  roomId: String;

}

@ObjectType()
export class ChatCredentials {
  @Field({ nullable: true })
  @Prop()
  chatUserId: string;

  @Field({ nullable: true })
  chatAuthToken: string;
}

@Schema()
@ObjectType({ description: 'from UserModel (src/users/models/user.ts)' })
export class User {
  @Field(() => ID)
  _id: MongooseTypes.ObjectId;

  @Field(() => String)
  @Prop()
  name: string;

  @Field(() => String)
  @Prop()
  surname: string;

  @Field(() => String, { nullable: true })
  @Prop()
  legalForm: string;

  @Field(() => String)
  @Prop()
  city: string;

  @Field(type => [Sector])
  @Prop()
  sector: Sector[];

  @Field(() => String)
  @Prop()
  eduLevel: string;

  @Prop(() => String)
  password: string;

  @Field(() => String)
  @Prop()
  type: string;

  @Field(() => String, { nullable: true })
  @Prop()
  email: string;

  @Field(() => String, { nullable: true })
  @Prop()
  website: string;

  @Field(() => String)
  @Prop()
  jobPosition: string;
  
  @Field(() => String)
  @Prop()
  lastJobTasks: string;
  
  @Field(() => String)
  @Prop()
  experience: string;

  @Field(() => [String])
  @Prop()
  languages: string[];

  @Field(() => [Competencies], { nullable: 'itemsAndList' })
  @Prop()
  competencies: Competencies[];

  @Field(() => [Softskill])
  @Prop()
  softSkills: Softskill[];

  @Field(() => Preferences, { nullable: true })
  @Prop()
  preferences: Preferences;

  @Field(() => String, {nullable: true})
  @Prop()
  avatarB64: string;

  @Field(() => String, {nullable: true})
  @Prop()
  video: string;

  @Field(() => String, {nullable: true})
  @Prop()
  cv: string;

  @Field(() => String, {nullable: true})
  @Prop()
  coverLetter: string;
  
  @Prop(() => String)
  token: string;

  @Prop()
  token_expiry: Date;

  @Field()
  @Prop()
  commsOK: Boolean;

  @Field()
  @Prop()
  privacyPolicyOK: Boolean;

  @Field(() => Date)
  @Prop()
  createdDate: Date;

  @Field(() => [Candidatures], { nullable: true })
  @Prop()
  candidatures: Candidatures[];

  @Field(() => Number)
  @Prop()
  maxMatchScore: Number;
  
  @Prop()
  chatPassword: string;

  @Field({ nullable: true })
  @Prop()
  chatUserId: string;

  @Field({ nullable: true })
  chatAuthToken: string;

  @Field(() => [Room], { nullable: true })
  @Prop()
  rooms: Room[]
}
export type UserDocument = User & Document;
export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.index({ lastJobTasks: 'text', jobPosition: 'text'}, { default_language: 'spanish'})

