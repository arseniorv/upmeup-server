import { CacheModule, Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersResolver } from './users.resolver';
import { User, UserSchema } from './models/user';
import { StorageService } from 'src/common/modules/s3storage/storageService';
import { RocketChatService } from 'src/common/rocketchat/rocketchat.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
    ]),
    CacheModule.register()
  ],
  providers: [UsersService, UsersResolver, StorageService, RocketChatService],
  exports: [UsersService]
})
export class UsersModule {}
