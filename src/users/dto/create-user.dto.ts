/* eslint-disable prettier/prettier */
/** Used for transfer data like create and update...*/
/* eslint-disable prettier/prettier */
import { Field, InputType, registerEnumType } from '@nestjs/graphql';
import { Prop, Schema } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { LegalForm } from 'src/common/types/legalForm';
import { Competencies } from 'src/competencies/models/competence';
import { Sector } from 'src/sectors/models/sector';
import { Softskill } from 'src/softskills/models/softskills';

export type UserDocument = CreateUserDto & mongoose.Document;

registerEnumType(LegalForm, { name: 'LegalForm'});

@Schema()
@InputType({ description: 'Create new User' })
export class CreateUserDto {
  @Field(() => String)
  @Prop()
  name: string;

  @Field(() => String)
  @Prop()
  surname: string;

  @Field(() => LegalForm, { nullable: true } )
  @Prop({ type: String, enum: LegalForm })
  legalForm!: LegalForm;

  @Field(() => String)
  @Prop()
  city: string;

  @Field(() => String, { nullable: true } )
  @Prop()
  website: string;

  @Field(() => [Sector])
  @Prop()
  sector: Sector[];
  
  @Field(() => String)
  @Prop()
  eduLevel: string;

  @Field()
  @Prop()
  password: string;

  @Field(() => String)
  @Prop()
  type: string;

  @Field(() => String)
  @Prop()
  email: string;

  @Field(() => String)
  @Prop()
  jobPosition: string;

  @Field(() => String)
  @Prop()
  lastJobTasks: string;

  @Field(() => String)
  @Prop()
  experience: string;

  @Field(() => [String])
  @Prop()
  languages: string[];
  
  @Field(() => [Competencies])
  @Prop()
  competencies: Competencies[];

/* 
  @Field(() => [String])
  @Prop()
  knowledge: string[];
*/

  @Field(() => [Softskill])
  @Prop()
  softSkills: Softskill[];

  @Field(() => Boolean )
  @Prop()
  commsOK: Boolean;

  @Field(() => Boolean )
  @Prop()
  privacyPolicyOK: Boolean;

  @Field(() => Date)
  @Prop()
  createdDate: Date;

  @Field(() => String, { nullable: true })
  @Prop()
  avatarB64: string;

}
