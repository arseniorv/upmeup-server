/* eslint-disable prettier/prettier */
/** Used for transfer data like create and update...*/
/* eslint-disable prettier/prettier */
import { Field, ObjectType } from '@nestjs/graphql';
import { Prop, Schema } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type UserDocument = userJWT & mongoose.Document;

@Schema()
@ObjectType({ description: 'User access token' })
export class userJWT {
  @Field(() => String)
  @Prop()
  token: string;
}
