/* eslint-disable prettier/prettier */
import { CACHE_MANAGER, Headers, HttpException, HttpStatus, Inject, Logger, UseGuards } from '@nestjs/common';
import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AuthGuard } from 'src/auth/auth.guard';
import { StorageService } from 'src/common/modules/s3storage/storageService';
import { S3Url } from 'src/common/types/s3Url';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserInput } from './dto/inputs/update-user.input';
import { userJWT } from './dto/user-jwt.dto';
import { ChatCredentials, User } from './models/user';
import { UsersService } from './users.service';
import { RocketChatService } from 'src/common/rocketchat/rocketchat.service';
import { Types } from 'mongoose';
import { Cache } from 'cache-manager';


@Resolver(() => User)
export class UsersResolver {
  constructor(
    private readonly uService: UsersService,
    private readonly storageService: StorageService,
    private readonly chatService: RocketChatService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) { }

  //Get logged user Info
  @UseGuards(AuthGuard)
  @Query(returns => User, { name: 'me' })
  async getLoggedUser(@Context() context) {
    Logger.debug(`Data of logged user requested for user ${context.user._id}`);
    const user = await this.uService.getUserById(context.user._id);
    if (!user.chatUserId) {
      const chatCredentials: ChatCredentials = await this.uService.createChatUser(user);
      user.chatUserId = chatCredentials?.chatUserId
    }
    return user;
  }

  @UseGuards(AuthGuard)
  @Query(returns => ChatCredentials, { name: 'getChatToken' })
  async getChatToken(@Context() context) {
    Logger.debug(`Data of logged user requested for user ${context.user._id}`);
    const user = await this.uService.getUserById(context.user._id);
    const credentials: ChatCredentials = await this.chatService.getChatCredentials(user.chatUserId, user.chatPassword);
    if (!credentials) {
      return await this.uService.createChatUser(user);
    } else {
      return credentials
    }
  }

  // User List to Logged
  // @UseGuards(AuthGuard)
  @Query(() => [User])
  async getUsers(): Promise<User[]> {
    return this.uService.getUsersList();
  }


  // User By ID
  @Query(() => User)
  @UseGuards(AuthGuard)
  async getUser(@Context() context, @Args('id') id: string) {
    const user = await this.uService.getUserById(id);
    if (context.user._id.toString() !== id) {
      user.rooms = user.rooms.filter(room => room.userId.equals(new Types.ObjectId(context.user._id)));
    }
    return user;
  }

  @Query(() => User)
  @UseGuards(AuthGuard)
  async getUserByEmail(@Args('email') email: string) {
    console.log('ENTRA EN RESOLVER; email: ', email);
    console.log('SALIDA DEL RESOLVER: ', await this.uService.getUserByEmail(email))
    return await this.uService.getUserByEmail(email);
  }



  // Get user auth JWT with email and password
  @Query(() => userJWT)
  async getJWTByEmailAndPassword(@Args('email') email: string, @Args('password') password: string) {
    Logger.log(`Log-in attempt with email: ${email}`);
    return { token: this.uService.createToken(email, password) }
  }

  @Query((returns) => S3Url, { name: 'downloadCvUrl' })
  @UseGuards(AuthGuard)
  async getCVPresignedUrl(@Context() context, @Args({ name: 'id', nullable: true }) id: string) {
    const user = await this.uService.getUserById(id ? id : context.user._id);
    return { presignedUrl: await this.storageService.getPresignedDownloadUrl(user._id.toString(), user.cv) };
  }

  @Query((returns) => S3Url, { name: 'downloadCoverLetterUrl' })
  @UseGuards(AuthGuard)
  async getCoverLetterPresignedUrl(@Context() context, @Args({ name: 'id', nullable: true }) id: string) {
    const user = await this.uService.getUserById(id ? id : context.user._id);
    return { presignedUrl: await this.storageService.getPresignedDownloadUrl(user._id.toString(), user.coverLetter) };
  }

  @UseGuards(AuthGuard)
  @Query(() => S3Url, { name: 'uploadUrl' })
  async getPresignedUploadUrl(@Context() context, @Args('filename') filename: string) {
    return { presignedUrl: this.storageService.getPresignedUploadUrl(context.user._id, filename) }
  }

  /**
   * Mutation: Create new User resolver
  */
  @Mutation(() => User)
  async createUser(@Args('createUserDto') createUserDto: CreateUserDto): Promise<User> {
    return this.uService.createUser(createUserDto);
  }


  @Query(() => Boolean, { name: 'emailExists' })
  async checkEmail(@Args('email') email: string) {
    return this.uService.emailExists(email);
  }


  /**
   * Mutation: Update User
   */
  @UseGuards(AuthGuard)
  @Mutation(() => User, { name: 'updateUser' })
  async updateUser(
    @Args('id') _id: string,
    @Args({ name: 'userInput', type: () => UpdateUserInput }) input: UpdateUserInput,
    @Context() context
  ) {
    return await this.uService.updateUser(context.user._id, input);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => User, { name: 'cv' })
  async updateCv(@Context() context, @Args({ name: 'filename', nullable: true }) filename: string) {
    return this.updateFileField(context, 'cv', filename);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => User, { name: 'coverLetter' })
  async updateCoverLetter(@Context() context, @Args({ name: 'filename', nullable: true }) filename: string) {
    return this.updateFileField(context, 'coverLetter', filename);
  }

  @Mutation(() => Boolean, { name: 'resetToken' })
  async generatePasswordResetToken(@Args({ name: 'email' }) email: string) {
    return this.uService.generatePasswordResetToken(email);
  }

  @Mutation(() => Boolean, { name: 'resetPassword' })
  async resetPassword(@Args({ name: 'token' }) token: string, @Args({ name: 'password' }) password: string) {
    return this.uService.resetPassword(token, password);
  }

  @Mutation(() => Boolean, { name: 'createChatRoomIfNotExists' })
  async createChatRoomIfNotExists(
    @Args({ name: 'loggedUserID' }) loggedUserID: string,
    @Args({ name: 'interlocutorID' }) interlocutorID: string) {
    return this.uService.createChatRoomIfNotExists(loggedUserID, interlocutorID);
  }

  private async updateFileField(context, field: string, filename: string) {
    const updateUser = new UpdateUserInput();
    if (filename) {
      updateUser[field] = filename;
      if (await this.storageService.checkObjectExists(context.user._id, updateUser[field])) {
        const user = await this.uService.getUserById(context.user._id)
        if (user[field] !== updateUser[field]) {
          if (user[field])
            this.storageService.removeObject(context.user._id, user[field]);
          return await this.uService.updateUser(context.user._id, updateUser);
        }
        return user;
      }
      throw new HttpException('Provided file not found in storage', HttpStatus.BAD_REQUEST);
    } else {
      const user = await this.uService.getUserById(context.user._id)
      this.storageService.removeObject(context.user._id, user[field]);
      updateUser[field] = null;
      return this.uService.updateUser(context.user._id, updateUser);
    }
  }

  @UseGuards(AuthGuard)
  @Mutation(() => User, {name: 'deleteUser'})
  async deleteUser(@Context() context){
    return await this.uService.deleteUser(context.user._id);
  }


}
