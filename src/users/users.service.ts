import { BadRequestException, HttpException, HttpStatus, Injectable, Headers, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { compareSync, hashSync } from 'bcrypt';
import { randomBytes } from 'crypto';
import { sign } from 'jsonwebtoken';
import { Model, Types } from 'mongoose';
import * as nodemailer from 'nodemailer';
import * as nunjucks from 'nunjucks';
import { RocketChatService } from 'src/common/rocketchat/rocketchat.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserInput } from './dto/inputs/update-user.input';
import { ChatCredentials, User, UserDocument } from './models/user';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserDeletedEvent } from 'src/events/events';


const HASH_ROUNDS = 10;

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private uModel: Model<UserDocument>,
    private configService: ConfigService,
    private chatService: RocketChatService,
    private eventEmitter: EventEmitter2
  ) { }

  /**
   * Get User By User Logged ID
   * @param _id
   * @returns
  */
  async getUsersList(): Promise<User[]> {
    try {
      return this.uModel.find();
    } catch (error) {
      throw new BadRequestException();
    }
  }

  /**
   * Get User By UserID
   * @param _id
   * @returns
   */
  async getUserById(_id: string): Promise<User> {
    Logger.debug(`Getting user data for user ${_id}`);
    return this.uModel.findById(_id);
  }

  async getUserByIdAndSearch(_id: string, search: string): Promise<any> {
    return this.uModel.findOne({ $or: [{ $text: { $search: search } }, { _id: _id }] }, { score: { $meta: "textScore" } });
  }

  /**
  * Get User By email
  * @param email
  * @returns User
  */
  async getUserByEmail(email: string): Promise<User> {
    Logger.log(`getUserByEmail : ${email}`) // debug
    return await this.uModel.findOne({ email: email });
  }

  async getUserByChatUserId(chatUserId: string): Promise<User> {
    return this.uModel.findOne({ chatUserId });
  }

  /**
   * Create new User:
   * - encrypt password
   * - check if email already exists
   *   - 400 if exists
   *   - create (save) if not
   * - send signup email (asychronously)
   *   - (if enabled)
   * -> returns created user object
   *
   **/
  async createUser(createUserDto: CreateUserDto): Promise<User> {

    // Encrypt password
    createUserDto.password = hashSync(createUserDto.password, HASH_ROUNDS)
    Logger.debug(`Creating new user [${createUserDto.email}]`)
    // console.debug(`Request info: ${JSON.stringify(createUserDto)}`) // debug

    // Check if a user with this mail already exists
    const existingUser = await this.uModel.find({ email: createUserDto.email });
    if (existingUser.length > 0) {
      Logger.error(`Returning Error ${HttpStatus.BAD_REQUEST} : BAD_REQUEST_EMAIL_EXISTS : ${createUserDto.email}`)
      throw new HttpException('BAD_REQUEST_EMAIL_EXISTS', HttpStatus.BAD_REQUEST);
    }

    if (createUserDto.type === '1' || createUserDto.type === '2') {
      Logger.log(`Legacy user input, updating type field`)
      createUserDto.type = createUserDto.type === '1' ? 'candidate' : 'company'
    }
    const createdUser = new this.uModel(createUserDto);

    const result = await createdUser.save() as any; // Saved into database
    Logger.log(`New user/company [${createdUser.email}] created:`);
    Logger.log(createdUser);
    await this.createChatUser(result);
    if (process.env.SIGNUP_MAIL_ENABLED === undefined || JSON.parse(process.env.SIGNUP_MAIL_ENABLED)) {
      this.sendSignupMail(createdUser)
    }
    else {
      Logger.log(`SIGNUP_MAIL_ENABLED = false ; not sending any mail`);
    }

    // Async - do not care about result of mail sending
    return result

  }

  async emailExists(email: string) {
    // Check if a user with this mail already exists
    var existingUser = await this.uModel.find({ email: email });
    return existingUser.length > 0 ? true : false;
  }

  async createToken(email: string, password: string) {
    Logger.debug(`Creating token for ${email}`) // debug
    const user = await this.uModel.findOne({ email });
    if (user && compareSync(password, user.password)) {
      Logger.debug(`Successful login attempt with email: ${email} for user ${user._id}`);
      return sign({ email: user.email, name: user.name, surname: user.surname, _id: user._id }, this.configService.get('JWT_SECRET'));
    }
    Logger.debug(`Failed login attempt with email: ${email}`);
    return ''
  }

  avatarB64resized: string;
  /**
   * Update User Info (except skills).
   * @param _id
   * @param updateUserInput
   * @returns
   */


  async updateUser(_id: string, updateUserInput: UpdateUserInput) {
    
    Logger.log(`Updating user [${_id}] with:\n${JSON.stringify(updateUserInput)}`)
    if (updateUserInput.avatarB64) {
      await this.processAvatar(updateUserInput);
      updateUserInput.avatarB64 = this.avatarB64resized;
    }
    const user = await this.uModel.findByIdAndUpdate(_id, updateUserInput, { new: true });
    if (updateUserInput.name || updateUserInput.surname || updateUserInput.email) {
      this.chatService.updateUser(user)
    }
    return user;
  }

  async processAvatar(updateUserInput: UpdateUserInput) {
    let base64Parts = updateUserInput.avatarB64.split(';base64,');
    let base64Image = base64Parts.pop();
    let base64Header = base64Parts.pop();
    const imageBuffer = Buffer.from(base64Image, 'base64')
    const sharp = require('sharp');
    const avatarResized = await sharp(imageBuffer)
      .resize(110, 110)
      .toBuffer('Buffer')
    const avatarB64Resized = `${base64Header};base64,${avatarResized.toString('base64')}`
    this.avatarB64resized = avatarB64Resized
  }

  async generatePasswordResetToken(email: string): Promise<Boolean> {
    const user = await this.uModel.findOne({ email });
    if (!user) {
      throw new HttpException('USER_NOT_FOUND', HttpStatus.NOT_FOUND);
    }
    const lang = user.preferences?.language ? user.preferences.language : 'es';

    const token = randomBytes(16).toString('hex');
    let token_expiry = new Date();
    token_expiry.setDate(token_expiry.getDate() + 1);
    await this.uModel.findByIdAndUpdate(user._id, { token, token_expiry });
    const mailerTransport = nodemailer.createTransport({
      pool: true,
      host: this.configService.get('MAIL_SMTP_SERVER'),
      port: this.configService.get('MAIL_SMTP_PORT') ? this.configService.get('MAIL_SMTP_PORT') : 465,
      secure: true, // use TLS
      auth: {
        user: this.configService.get('MAIL_SMTP_USER'),
        pass: this.configService.get('MAIL_SMTP_PASSWORD'),
      },
    });

    const CLIENT_URL = process.env.CLIENT_URL || 'https://upmeup.io';
    nunjucks.configure({ autoescape: true });
    const message = {
      from: "hola@upmeup.es",
      to: user.email,
      subject: "Password Reset",
      text: nunjucks.render(`src/templates/password-reset/${lang}.txt`, { name: user.name, resetUrl: `${CLIENT_URL}/password-reset/${token}` }),
      html: nunjucks.render(`src/templates/password-reset/${lang}.html`, { name: user.name, resetUrl: `${CLIENT_URL}/password-reset/${token}` })

    };
    await mailerTransport.sendMail(message);
    return true;
  }

  async resetPassword(token: string, password: string) {
    const user = await this.uModel.findOne({ token });

    if (!user) throw new HttpException('TOKEN_NOT_FOUND', HttpStatus.NOT_FOUND);;
    if (user.token_expiry < new Date()) {
      await this.uModel.findByIdAndUpdate(user._id, { $unset: { token: '', token_expiry: '' } });
      throw new HttpException('TOKEN_EXPIRED', HttpStatus.AMBIGUOUS);
    }

    await this.uModel.findByIdAndUpdate(user._id, { password: hashSync(password, HASH_ROUNDS), $unset: { token: '', token_expiry: '' } });
    return true;
  }

  sendSignupMail(createdUser: User) {
    // const lang = createdUser.preferences?.language ? createdUser.preferences.language : 'es';
    const mailerTransport = nodemailer.createTransport({
      pool: true,
      host: this.configService.get('MAIL_SMTP_SERVER'),
      port: this.configService.get('MAIL_SMTP_PORT') ? this.configService.get('MAIL_SMTP_PORT') : 465,
      secure: true, // use TLS
      auth: {
        user: this.configService.get('MAIL_SMTP_USER'),
        pass: this.configService.get('MAIL_SMTP_PASSWORD'),
      },
    });

    nunjucks.configure({ autoescape: true });

    const userType = createdUser.type === 'candidate' ? 'signup-confirmation' : 'signup-company-confirmation';

    const message = {
      from: "hola@upmeup.es",
      to: createdUser.email,
      subject: "UpmeUp",
      // text: nunjucks.render(`src/templates/${userType}/${lang}.txt`),
      // html: nunjucks.render(`src/templates/${userType}/${lang}.html`)
      text: nunjucks.render(`src/templates/${userType}/es.txt`),
      html: nunjucks.render(`src/templates/${userType}/es.html`)
    };

    Logger.debug(`Sending signup mail to : [${createdUser.email}]`)
    mailerTransport.sendMail(message);

  }

  async createChatUser(user: User) {
    user.chatPassword = randomBytes(16).toString('hex');
    const chatCredentials: ChatCredentials = await this.chatService.createUser(user);
    await this.uModel.findByIdAndUpdate(user._id, { chatPassword: user.chatPassword, chatUserId: chatCredentials.chatUserId });
    return chatCredentials
  }

  async createChatRoomIfNotExists(userId: string, recipientId: string): Promise<Boolean> {
    Logger.debug(`Creating rocket.chat room for ${userId} and ${recipientId}`);
    const userInfo = await this.getUserById(userId);
    const recipientInfo = await this.getUserById(recipientId);
    if(!recipientInfo.chatUserId) {
      recipientInfo.chatUserId = (await this.createChatUser(recipientInfo))?.chatUserId;
    }
    const userHasRoom = userInfo.rooms && userInfo.rooms.findIndex(room => room.userId.equals(new Types.ObjectId(recipientId))) !== -1;
    const recipientHasRoom = recipientInfo.rooms && recipientInfo.rooms.findIndex(room =>  room.userId.equals(new Types.ObjectId(userId))) !== -1
    if (!userHasRoom || !recipientHasRoom) {
      const chatRoom = await this.chatService.createConversation(userInfo.chatUserId, userInfo.chatPassword, recipientInfo.chatUserId);
      if (!chatRoom)
        return false;
      if (!userHasRoom) {
        Logger.debug(`Assigning room ${chatRoom} to user ${userId}`);
        await this.uModel.findByIdAndUpdate(userInfo._id, { rooms: [...userInfo.rooms, { userId: recipientInfo._id, roomId: chatRoom }] }, {new: true});
      }
      if (!recipientHasRoom) {
        Logger.debug(`Assigning room ${chatRoom} to user ${recipientId}`);
        await this.uModel.findByIdAndUpdate(recipientInfo._id, { rooms: [...recipientInfo.rooms, { userId: userInfo._id, roomId: chatRoom }] }, { new: true});
      }
    }
    return true;
  }

  //Delete user:
  // - Delete from offers [done]
  // - Delete from candidates array inside Offers [done]
  // - Delete from projects [done]
  // - Delete from interested Users array inside Projects [done]
  // - Delete from admins array inside Projects [done]
  // - Delete chat tokens [done]
  // - Delete user [done]
  async deleteUser (_id: string){
    const user = await this.getUserById(_id);
    const deletedUser = await this.uModel.findByIdAndDelete(_id);
    const event = new UserDeletedEvent(user);
    this.eventEmitter.emit('user.deleted', event);
    return user
 

  }
}
